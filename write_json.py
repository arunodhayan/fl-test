import json


dict={
 "print_freq": 100,
 "num_workers":4,
 "model_name": "resnext50_32x4d",
 "dim":(256, 256),
 "scheduler":"CosineAnnealingWarmRestarts",
 "epochs":10,
 "lr":1e-4,
 "T_0":10, 
 "min_lr":5e-7, 
 "batch_size":64,
 "weight_decay":1e-6,
 "max_grad_norm":1000,
 "seed":42,
 "target_size":2,
 "target_col":"labels",
 "n_fold": 5,
 "pretrained": True
}



json_object = json.dumps(dict, indent = 4)
  
# Writing to sample.json
with open("Hyperparams.json", "w") as outfile:
    outfile.write(json_object)
