import os
import math
import time
import random
import shutil
from pathlib import Path
from contextlib import contextmanager
from collections import defaultdict, Counter
import scipy as sp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import StratifiedKFold
from tqdm.auto import tqdm
from functools import partial
import cv2
from PIL import Image
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam, SGD
import torchvision.models as models
from torch.nn.parameter import Parameter
from torch.utils.data import DataLoader, Dataset
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts
import albumentations as A
from albumentations.pytorch import ToTensorV2
from albumentations import ImageOnlyTransform
import timm
import warnings 
warnings.filterwarnings('ignore')
import torch
from torchvision import datasets, transforms, models

class CFG:
    print_freq=100
    num_workers=4
    model_name= 'resnext50_32x4d'
    dim=(256, 256)
    scheduler='CosineAnnealingWarmRestarts'
    epochs=10
    lr=1e-4
    T_0=10 # for CosineAnnealingWarmRestarts
    min_lr=5e-7 # for CosineAnnealingWarmRestarts
    batch_size=32
    weight_decay=1e-6
    max_grad_norm=1000
    seed=42
    target_columns={'unterbrochen':0,'erhalten':1},
    threshold=0.5
    target_size=2
    target_col='labels'
    n_fold = 5
    pretrained = True
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    data_dir='train/erhalten/1002655_00149313_SLICE_008_L_OCT_RAW.png'

target_columns= {'unterbrochen':0,'erhalten':1} #CFG.target_columns
INV_CODE = {v: k for k, v in target_columns.items()}

test_transforms= transforms.Compose([
            transforms.Resize(CFG.dim[0], CFG.dim[1]),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])

data=CFG.data_dir
data={'filename':data,'labels':0}
test = pd.DataFrame(data,index =[0])
#print (test)
#test = pd.read_csv('train_metadata.csv', delimiter=',', encoding="utf-8",skiprows=[i for i in range(2,7894)])



#test['labels'] = test['labels'].replace('unterbrochen',0)
#test['labels'] = test['labels'].replace('erhalten',1)

'''
test.loc[test['labels']==0, 'filepath'] = 'train/erhalten/' +  test.query('labels==0')['filename'] 
test.loc[test['labels']==1, 'filepath'] = 'train/unterbrochen/' + test.query('labels==1')['filename'] 
'''

test.loc[test['labels']==0, 'filepath'] =  test.query('labels==0')['filename'] 


test = test.dropna().reset_index(drop=True)
print (test)

class TestDataset(Dataset):
    def __init__(self, df, transform=None):
        self.df = df
        self.file_paths = df['filepath'].values
        self.labels = df['labels'].values
        self.transform = transform
        
    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        file_name = self.file_paths[idx]
        file_path = file_name
        
        image=cv2.imread(file_path)

        image = np.squeeze(image)

        #if self.transform:
        image = self.transform(image=image)
        image = image['image']
        label = torch.tensor(self.labels[idx]).long()
        return image, label

def get_transforms(*, data):
    
    if data == 'train':
        return A.Compose([
            A.Resize(CFG.dim[0], CFG.dim[1]),
            A.HorizontalFlip(p=0.5),
            A.VerticalFlip(p=0.5),
#            A.augmentations.transforms.JpegCompression(p=0.5),
#            A.augmentations.transforms.ImageCompression(p=0.5, compression_type=A.augmentations.transforms.ImageCompression.ImageCompressionType.WEBP),
            A.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])

    elif data == 'valid':
        return A.Compose([
            A.Resize(CFG.dim[0], CFG.dim[1]),
            A.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])











class CustomResNext(nn.Module):
    def __init__(self, model_name='resnext50_32x4d', pretrained=False):
        super().__init__()
        self.model = timm.create_model(model_name, pretrained=pretrained)
        n_features = self.model.fc.in_features
        self.model.fc = nn.Linear(n_features, CFG.target_size)

    def forward(self, x):
        x = self.model(x)
        return x




####################################################################################################

def inference(images,device):
    model = CustomResNext(CFG.model_name, pretrained=False)
    model.to(device)
    #resnext50_32x4d_ensembled_best.pth
    checkpoint = torch.load('Ensembled/resnext50_32x4d_ensembled_best.pth', map_location='cpu')
    model.load_state_dict(checkpoint["model"])
    model.to(device)
    model.eval()
    #print (images)
    images = images.to(device)
    
    
    
    with torch.no_grad():
         proba = model(images)
    events = proba >= threshold
    labels = np.argwhere(events).reshape(-1)
    return labels


def inference_1(test_loader, device):
    model = CustomResNext(CFG.model_name, pretrained=False)
    model.to(device)
    #state = torch.load('output/resnext50_32x4d_best.pth', map_location='cpu')
    state = torch.load('Ensembled/resnext50_32x4d_ensembled_best.pth', map_location='cpu')
    tk0 = tqdm(enumerate(test_loader), total=len(test_loader))
    #probs = []
    for i, (images,labels) in tk0:
    
        images = images.to(device)
        
        
        model.load_state_dict(state['model'])
        model.eval()
        with torch.no_grad():
             y_preds = model(images)
             y_preds=y_preds.softmax(1).to('cpu').numpy()
        threshold=0.5               
        events = y_preds >= threshold
        print (y_preds)
        probs = np.argwhere(events).reshape(-1)
        for ci in probs:
         probs=INV_CODE[ci]       
        #print (probs)         
        return probs

#################################################################################################

valid_folds=test


test_dataset = TestDataset(test, 
                                 transform= get_transforms(data='valid'))
test_loader = DataLoader(test_dataset,batch_size=1)



probs=inference_1(test_loader,CFG.device)
print (probs)


