import os
OUTPUT_DIR = 'output_1/'
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)
from numpy import *
from sklearn.model_selection import StratifiedKFold
import pandas as pd
import os
import math
import time
import random
import shutil
from pathlib import Path
from contextlib import contextmanager
from collections import defaultdict, Counter
import scipy as sp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import StratifiedKFold
from tqdm.auto import tqdm
from functools import partial
import cv2
from PIL import Image
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam, SGD
import torchvision.models as models
from torch.nn.parameter import Parameter
from torch.utils.data import DataLoader, Dataset
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts
import albumentations as A
from albumentations.pytorch import ToTensorV2
from albumentations import ImageOnlyTransform
import timm
import warnings 
import cv2
from torchvision.datasets import ImageFolder

import math
import time
import random
import shutil
from pathlib import Path
from contextlib import contextmanager
from collections import defaultdict, Counter
import scipy as sp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import StratifiedKFold
from tqdm.auto import tqdm
from functools import partial
import cv2
from PIL import Image
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam, SGD
import torchvision.models as models
from torch.nn.parameter import Parameter
from torch.utils.data import DataLoader, Dataset
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts
import albumentations as A
from albumentations.pytorch import ToTensorV2
from albumentations import ImageOnlyTransform
import timm
import warnings 
warnings.filterwarnings('ignore')
import torch
from torchvision import datasets, transforms, models

warnings.filterwarnings('ignore')


class CFG:
    print_freq=100
    num_workers=4
    model_name= 'resnext50_32x4d'
    dim=(256, 256)
    scheduler='CosineAnnealingWarmRestarts'
    epochs=10
    lr=1e-4
    T_0=10 # for CosineAnnealingWarmRestarts
    min_lr=5e-7 # for CosineAnnealingWarmRestarts
    batch_size=32
    weight_decay=1e-6
    max_grad_norm=1000
    seed=42
    target_columns={'unterbrochen':0,'erhalten':1},
    threshold=0.5
    target_size=2
    target_col='labels'
    n_fold = 5
    pretrained = True
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    data_dir='train/erhalten/1002655_00149313_SLICE_008_L_OCT_RAW.png'

target_columns= {'unterbrochen':0,'erhalten':1} #CFG.target_columns
INV_CODE = {v: k for k, v in target_columns.items()}

test_transforms= transforms.Compose([
            transforms.Resize(CFG.dim[0], CFG.dim[1]),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])

data=CFG.data_dir
data={'filename':data,'labels':0}
test = pd.DataFrame(data,index =[0])

test.loc[test['labels']==0, 'filepath'] =  test.query('labels==0')['filename'] 


test = test.dropna().reset_index(drop=True)
print (test)


class TestDataset(Dataset):
    def __init__(self, df, transform=None):
        self.df = df
        self.file_paths = df['filepath'].values
        self.labels = df['labels'].values
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        file_name = self.file_paths[idx]
        file_path = file_name

        image=cv2.imread(file_path)

        image = np.squeeze(image)

        #if self.transform:
        image = self.transform(image=image)
        image = image['image']
        label = torch.tensor(self.labels[idx]).long()
        return image, label

def get_transforms(*, data):
    
    if data == 'train':
        return A.Compose([
            A.Resize(CFG.dim[0], CFG.dim[1]),
            A.HorizontalFlip(p=0.5),
            A.VerticalFlip(p=0.5),
#            A.augmentations.transforms.JpegCompression(p=0.5),
#            A.augmentations.transforms.ImageCompression(p=0.5, compression_type=A.augmentations.transforms.ImageCompression.ImageCompressionType.WEBP),
            A.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])

    elif data == 'valid':
        return A.Compose([
            A.Resize(CFG.dim[0], CFG.dim[1]),
            A.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
            ToTensorV2(),
        ])


class CustomResNext(nn.Module):
    def __init__(self, model_name='resnext50_32x4d', pretrained=True):
        super().__init__()
        self.model = timm.create_model(model_name, pretrained=pretrained)
        n_features = self.model.fc.in_features
#        print (n_features)
        self.model.fc = nn.Linear(n_features, CFG.target_size)

    def forward(self, x):
        x = self.model(x)
        return x



class MyEnsemble(nn.Module):
    def __init__(self, modelA, modelB):
        super(MyEnsemble, self).__init__()
        self.modelA = modelA
        self.modelB = modelB
        self.classifier = nn.Linear(4, 2)
        
    def forward(self, x1, x2):
        x1 = self.modelA(x1)
        x2 = self.modelB(x2)
        x = torch.cat((x1, x2), dim=1)
        x = self.classifier(F.relu(x))
        return x


model_A = CustomResNext(CFG.model_name, pretrained=False)
model_B = CustomResNext(CFG.model_name, pretrained=False)

checkpoint_A = torch.load('output/resnext50_32x4d_best.pth', map_location='cpu')
checkpoint_B = torch.load('output_1/resnext50_32x4d_best.pth', map_location='cpu')

model_A.load_state_dict(checkpoint_A["model"])
model_B.load_state_dict(checkpoint_B["model"])

#model_new=MyEnsemble(model_A,model_B)


OUTPUT_DIR='Ensembled_1/'





#if not os.path.exists(OUTPUT_DIR):
#    os.makedirs(OUTPUT_DIR)
#torch.save({'model': model_new.state_dict()},OUTPUT_DIR+f'{CFG.model_name}_ensembled_best.pth')



################Predict###########################################

def inference_1(test_loader, device):
    #model = CustomResNext(CFG.model_name, pretrained=False)
    model=MyEnsemble(model_A,model_B)

    model.to(device)
    #state = torch.load('output/resnext50_32x4d_best.pth', map_location='cpu')
    state = torch.load('Ensembled/resnext50_32x4d_ensembled_best.pth', map_location='cpu')
    tk0 = tqdm(enumerate(test_loader), total=len(test_loader))
    #probs = []
    for i, (images,labels) in tk0:
    
        images = images.to(device)


        model.load_state_dict(state['model'])
        model.eval()
        with torch.no_grad():
             y_preds = model(images,images)
             y_preds=y_preds.softmax(1).to('cpu').numpy()
        threshold=0.5
        events = y_preds >= threshold
        print (y_preds)
        probs = np.argwhere(events).reshape(-1)
        for ci in probs:
         probs=INV_CODE[ci]       
        #print (probs)         
        return probs



valid_folds=test


test_dataset = TestDataset(test, 
                                 transform= get_transforms(data='valid'))
test_loader = DataLoader(test_dataset,batch_size=1)



probs=inference_1(test_loader,CFG.device)
print (probs)
